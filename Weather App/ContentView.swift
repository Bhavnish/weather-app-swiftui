//
//  ContentView.swift
//  Weather App
//
//  Created by cqlapple on 09/06/21.
//

import SwiftUI

struct ContentView: View {
    
    @State private var isNight = false
    
    var body: some View {
        ZStack {
            BackgoroundView(isNight: $isNight)
            VStack(spacing: 15){
                
                CityNameView(cityname: "Chandigarh , IN")
                Spacer()
                CityViewTempture(temptureImage: "039-sun", tempture: 35)
                Spacer()
                Spacer()
                HStack( spacing: 10){
                    WeatherDaysView(dayofWeek: "SUN", imageName: "039-sun", tempture: "26°")
                    WeatherDaysView(dayofWeek: "MON", imageName: "002-cloud-1", tempture: "30°")
                    WeatherDaysView(dayofWeek: "TUE", imageName: "039-sun", tempture: "28°")
                    WeatherDaysView(dayofWeek: "WED", imageName: "036-storm-4", tempture: "26°")
                    WeatherDaysView(dayofWeek: "THU", imageName: "039-sun", tempture: "45°")
                    WeatherDaysView(dayofWeek: "FRI", imageName: "045-thunder", tempture: "26°")
                }
                
                Spacer()
                
                Button(action: {
                    print("Change Tapped!")
                    isNight.toggle()
                }, label: {
                    Text("Change The Days")
                        .frame(width: 250, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .background(Color.white)
                        .foregroundColor(.blue)
                        .font(.system(size: 18, weight: .bold, design: .default))
                        .cornerRadius(10)
                })
                Spacer()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
            
        }
        
        
    }
}

struct WeatherDaysView: View {
    
    var dayofWeek:String
    var imageName:String
    var tempture:String
    var body: some View {
        VStack{
            VStack{
                Text(dayofWeek)
                    .font(.system(size: 18, weight: .medium, design: .default))
                    .foregroundColor(.white)
                    .bold()
                Image(imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                Text(tempture)
                    .font(.system(size: 18, weight: .medium, design: .default))
                    .foregroundColor(.white)
                    .bold()
            }
            .padding(8)
        }
        
    }
}

struct BackgoroundView: View {
    
    @Binding var isNight:Bool
    
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [isNight ? .black : Color.purple,   isNight ? .gray : Color.init("lightblue")]),
                       startPoint: .topLeading,
                       endPoint: .bottomTrailing)
            .edgesIgnoringSafeArea(.all)
    }
}



struct CityViewTempture: View {
    
    var temptureImage:String
    var tempture:Int
    
    var body: some View {
        VStack(spacing: 10){
            Image(temptureImage)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 120, height: 120, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            Text("\(tempture)°")
                .font(.system(size: 50, weight: .medium, design: .default))
                .foregroundColor(.white)
                .bold()
        }
    }
}

struct CityNameView: View {
    var cityname:String
    var body: some View {
        Text(cityname)
            .foregroundColor(.white)
            .bold()
            .font(.system(size: 35, weight: .medium, design: .default))
            .frame(width: .some(350), height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, 2)
    }
}
