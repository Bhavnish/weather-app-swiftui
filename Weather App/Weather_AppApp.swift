//
//  Weather_AppApp.swift
//  Weather App
//
//  Created by cqlapple on 09/06/21.
//

import SwiftUI

@main
struct Weather_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
